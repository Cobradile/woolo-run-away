#ifndef __SPRITES_H__
#define __SPRITES_H__

#include "pm.h"
#include <stdint.h>

#define WOOLOO1 4
#define WOOLOO2 5
#define WOOLOO3 6
#define WOOLOO4 7

#define SOLID_OBSTACLES 2
#define NUM_OBSTACLES 4

#define sprites1 0x010400
#define sprites2 0x010600
#define SET_PRC_SPR(x) \
	PRC_SPR_HI = (x & 0xFF0000) >> 16; \
	PRC_SPR_MID = (x & 0xFF00) >> 8; \
	PRC_SPR_LO = x & 0xFF

extern const _rom uint8_t obstacles_sprites1[256];
extern const _rom uint8_t obstacles_sprites2[256];
extern const _rom uint8_t char_sprites1[256];
extern const _rom uint8_t char_sprites2[256];

#endif // __SPRITES_H__
