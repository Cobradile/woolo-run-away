#include "mapgen.h"

#include "debug.h"
#include "sprites.h"
#include "tiles.h"
#include "wyhash16.h"

#define BG_LEGAL_OF(ti) (bg_legal + ti)
const uint8_t _rom bg_legal[] _at(0x5000) = {
	// Use BG_LEGAL_OF(tile index) to convert to what tile
	// can legally follow it. This can be upgraded to have
	// several random legal tiles for each.
	// Each "tile" is a column of two tiles, matched below
	1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0
};

const uint8_t _rom bg_lower[] _at(0x5100) = {
	12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23
};

uint8_t mapstate = 0, nlt = 0, parallax = 0;

struct {
	int8_t oam_idx;
	uint8_t x;
	uint8_t y;
	uint8_t tile;
} obstacles[] = {
	{-1, 0, 0, 0},
	{-1, 0, 0, 0},
	{-1, 0, 0, 0},
	{-1, 0, 0, 0},
	{-1, 0, 0, 0},
	{-1, 0, 0, 0},
};
uint8_t num_obstacles = 0;
#define MAX_OBSTACLES 6

int8_t obstacle2oam(uint8_t idx) {
	return obstacles[idx].oam_idx;
}

void swap_grays() {
	if (mapstate) {
		PRC_MAP = tiles1;
		SET_PRC_SPR(sprites1);
	}
	else {
		PRC_MAP = tiles2;
		SET_PRC_SPR(sprites2);
	}
	mapstate ^= 1;
}

void init_map(uint16_t seed) {
	uint8_t i, result;

	// Since we're repeating a 12x2-tile pattern in a 24 tile width screen
	// we can set both tm1 and tm2 as we loop thru the tile indexes.
	volatile unsigned char *tm1 = TILEMAP, *tm2 = TILEMAP + 12;

	wh16seed(seed);
	
	// Map needs to be 24x8
    PRC_MODE = COPY_ENABLE|SPRITE_ENABLE|MAP_ENABLE|MAP_24X8;
	PRC_MAP = tiles1;
	SET_PRC_SPR(sprites1);
	OAM[0].ctrl = OAM_ENABLE;  // default player sprite

	//dprintf("obstacle map addr: %i\n", (uint16_t)obstacles);

	// Tiles 0~23 (inclusive) are the forest BG
	for (i = 0; i < 12; ++i, ++tm1, ++tm2) {
		*tm1 = i;
		*tm2 = i;
	}
	// tm2 has looped to the start of the second row now
	tm1 = tm2 + 12;
	for (; i < 24; ++i, ++tm1, ++tm2) {
		*tm1 = i;
		*tm2 = i;
	}

	// BG set, set the starting ground tiles
	for (; i < 48; ++i, ++tm1) *tm1 = 24;
	for (; i < 24 * 8; ++i, ++tm1) *tm1 = 25;

	// Generate obstacles
	for (i = 0; i < MAX_OBSTACLES; ++i) {
		result = generate_obstacle(i);
		if (!result) break;
	}
	num_obstacles = i;
}

void move_map(uint8_t pixels) {
	uint8_t i, x, y, mapx, remove = 0, result;
	int8_t o;
	unsigned char *tm1, *tm2;

	swap_grays();

	if (!pixels) return;
	
	for (i = 0; i < num_obstacles; ++i) {
		o = obstacles[i].oam_idx;
		if (pixels > obstacles[i].x) {
			// Subtraction would cause an overflow
			// i.e. sprite is out of frame
			OAM[o].ctrl = 0;  // we can assume it's allocated
			//dprintf("Gotta destroy %i (OAM %i)\n", (uint16_t)i, (int16_t)o);
			++remove;
			continue;
		}

		obstacles[i].x -= pixels;
		if (o >= 0) {
			OAM[o].x -= pixels;
		}
		else if (obstacles[i].x < 128) {
			allocate_obstacle(i);
		}
	}
	
	if (remove) {
		////dprintf("Removing %i from the front\n", (uint16_t)remove);
		for (i = 0; remove < num_obstacles; ++i, ++remove) {
			obstacles[i].oam_idx = obstacles[remove].oam_idx;
			obstacles[i].x = obstacles[remove].x;
			obstacles[i].y = obstacles[remove].y;
			obstacles[i].tile = obstacles[remove].tile;
		}
		num_obstacles = i;
	}

	// Generate new obstacles
	if (i < MAX_OBSTACLES) {
		////dprintf("New obstacles on move starting from %i\n", i);
		for (; i < MAX_OBSTACLES; ++i) {
			result = generate_obstacle(i);
			if (!result) break;
		}
		num_obstacles = i;
	}

	// Scroll the BG 1 pixel for every 4 pixels the sprites move
	parallax += pixels;

	if (parallax > 4) {
		PRC_SCROLL_X += parallax / 4;
		parallax %= 4;
	}
	else {
		return;
	}

	mapx = (uint8_t)PRC_SCROLL_X / 8;
	if (mapx == 0) return;

	// Copy tiles to lower area as we can
	if (mapx < 10) {
		tm1 = TILEMAP + (mapx - 1);
		tm2 = tm1 + 10;

		for (y = 0; y < 8; ++y, tm1 += 24, tm2 += 24) {
			*tm1 = *tm2;
		}
	}
	else {
		tm1 = TILEMAP + 9;
		tm2 = tm1 + 10;

		for (y = 0; y < 8; ++y, tm1 += 21, tm2 += 21) {
			for (x = 0; x < 3; ++x, ++tm1, ++tm2) {
				*tm1 = *tm2;
			}
		}

		PRC_SCROLL_X -= 10 * 8;

		// Then generate new tiles for the last mapx tiles
		tm1 = TILEMAP + 11;
		tm2 = tm1 + 1;
		for (x = 12; x < 24; ++x, ++tm1, ++tm2) {
			*tm2 = *BG_LEGAL_OF(*tm1);
			*(tm2 + 24) = bg_lower[*tm2];
		}
	}
}

void exchange_obstacle(uint8_t player_id, uint8_t obstacle_id) {
	uint8_t i, tmp;
	oam_sprite_t *player = &OAM[player_id];
	oam_sprite_t *obstacle = &OAM[obstacles[obstacle_id].oam_idx];
	dprintf("Exchange %u with %u (%u OAM)\n", (uint16_t)player_id, (uint16_t)obstacle_id, (uint16_t) obstacles[obstacle_id].oam_idx);
	tmp = player->x; player->x = obstacle->x; obstacle->x = tmp;
	tmp = player->y; player->y = obstacle->y; obstacle->y = tmp;
	tmp = player->tile; player->tile = obstacle->tile; obstacle->tile = tmp;
	tmp = player->ctrl; player->ctrl = obstacle->ctrl; obstacle->ctrl = tmp;

	obstacles[obstacle_id].oam_idx = player_id;
}

uint8_t generate_obstacle(uint8_t idx) {
	uint16_t n = wyhash16();
	uint8_t x, xr, rightmost_x;

	rightmost_x = idx ? obstacles[idx-1].x : 12 * 8;

	// Random x between 32~80 pixels away; must be outside the map
	if (rightmost_x >= 256 - 48) return 0;
	x = rightmost_x + 16 + 32; // 16 for the width
	xr = wyhash16() % (80-32);
	//dprintf("Generating %u, x: %u, xr: %u\n", (uint16_t)idx, (uint16_t)x, (uint16_t)xr);
	if (x < 12 * 8 + 16) x = 12 * 8 + 16;
	else if (x >= 256 - xr) {
		// Would overflow, so don't generate...
		return 0;
	}
	obstacles[idx].x = x + xr; 

	// Random y between 32~64
	obstacles[idx].y = 32 + wyhash16() % 32;

	// Determine which obstacle to use
	obstacles[idx].tile = n % NUM_OBSTACLES;

	//dprintf("Sprite %i is obstacle %i\n", idx, obstacles[idx].tile);
	return 1;
}

void allocate_obstacle(uint8_t idx) {
	uint8_t i;

	// Find an open index, -1 if there aren't any
	for (i = 0; i < 24 && OAM[i].ctrl & OAM_ENABLE; ++i);
	//dprintf("Allocating %u to OAM %u\n", (uint16_t)idx, (uint16_t)i);
	if (i == 24) return;

	obstacles[idx].oam_idx = i;
	OAM[i].x = obstacles[idx].x;
	OAM[i].y = obstacles[idx].y;
	OAM[i].tile = obstacles[idx].tile;
	OAM[i].ctrl = OAM_ENABLE;
}

uint8_t obstacle_collides(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t speed) {
	uint8_t i, r = x + w, b = y + h;
	uint8_t next = r + speed;
	uint8_t result = 0;

	for (i = 0; i < num_obstacles; ++i) {
		oam_sprite_t *obstacle;
		if (obstacles[i].oam_idx < 0) break;

		obstacle = &OAM[obstacles[i].oam_idx];
		if (obstacle->x > next) break;
		result = i;

		if (obstacle->x >= r) {
			// Will collide w/ col next turn, may collide right
			result |= COLLIDES_ROW_RIGHT;
		}
		else if (obstacle->x + 16 < x) {
			// Already passed by
			continue;
		}

		//dprintf("%u (OAM %u) in collision range", (uint16_t)i, (uint16_t)obstacles[i].oam_idx);
		result |= COLLIDES_COL;

		if (b >= obstacle->y + 4 && y <= obstacle->y + 6) {
			// 4 pixel overlap
			if (obstacle->y < y) {
				//dprint_str(", collides above player");
				result |= OVERLAPS_ROW_ABOVE;
			} else {
				//dprint_str(", collides below player");
			}
		}
		else {
			// No row collision, disable this if it was set.
			result &= ~COLLIDES_ROW_RIGHT;
			//dprint_str(", no collision");
		}

		if (obstacle->tile < SOLID_OBSTACLES) {
			//dprint_str(", is solid");
			result |= OBSTACLE_SOLID;
		}

		//dprint_char('\n');
		return result;
	}
	return 0;
}
