#include "pm.h"
#include <stdint.h>
#include "sprites.h"
#include "mapgen.h"
#include "wolootitle.h"

uint8_t player = 0;

int main(void)
{
	uint8_t i, keys, collision, obstacle, obstacle_y, obstacle_oam;
	uint8_t speed = 2, effective_speed;
	int8_t update_player;
	SET_PRC_SPR(sprites1);
	
	// TODO: intro screen
	
	PRC_MODE = COPY_ENABLE|SPRITE_ENABLE|MAP_ENABLE|MAP_12X16;
	PRC_RATE = RATE_36FPS;
	PRC_MAP = wolootitle_tiles1;

	for (i = 0; i < 12 * 8; ++i) {
		TILEMAP[i] = i;
	}

    TMR1_OSC = 0x13; // Use Oscillator 2 (31768Hz)
    TMR1_SCALE = 0x08 | 0x02 | 0x80 | 0x20; // Scale 2 (8192 Hz)
    TMR1_CTRL = 0x86; // Enable timer 2 at 0 in 16 bit mode
	
	for(i = 0;; i ^= 1) {
		PRC_MAP = i ? wolootitle_tiles1 : wolootitle_tiles2;
		wait_vsync();

		if (~KEY_PAD & KEY_A) {
            TMR1_CTRL = 0; // Pause timer
			init_map((uint16_t) TMR1_CNT);
			break;
		}
	}
	
	// Set up player sprite's initial position
	OAM[player].x = 32;
	OAM[player].y = 40;
	OAM[player].tile = WOOLOO1;
	OAM[player].ctrl = OAM_ENABLE | OAM_FLIPH;
	
	for(;;) {
		update_player = -1;
		wait_vsync();
		// Should be multiples of 2 for grays to work, under 16

		keys = ~KEY_PAD;
		
		collision = obstacle_collides(OAM[player].x, OAM[player].y, 16, 16, speed);
		effective_speed = speed;
		if (collision & COLLIDES_COL) {
			obstacle = collision & OBSTACLE_IDX;
			obstacle_oam = obstacle2oam(obstacle);
			obstacle_y = OAM[obstacle_oam].y;
			if (collision & COLLIDES_ROW_RIGHT) {
				// TODO: elastic camera
				if (collision & OBSTACLE_SOLID) {
					if (OAM[player].x + 16 >= OAM[obstacle_oam].x) {
						effective_speed = 0;
					} else {
						effective_speed = OAM[obstacle_oam].x - (OAM[player].x + 16);
					}
				} else {
					effective_speed = speed >> 1;
					if (!effective_speed) {
						effective_speed = 1;
					}
				}
			}
			if (collision & OVERLAPS_ROW_ABOVE) {
				if (obstacle_oam < player) {
					// Player should render on top of the obstacle,
					// so needs a lower index
					exchange_obstacle(player, obstacle);
					player = obstacle_oam;
				}
			}
			else {
				if (obstacle_oam > player) {
					// Player should render underneath the obstacle,
					// so needs a higher index
					exchange_obstacle(player, obstacle);
					player = obstacle_oam;
				}
			}
		}

		if (keys & KEY_UP) 
		{
			if (OAM[player].y > 32
				&& (!effective_speed
					|| !(collision & OBSTACLE_SOLID)
					|| OAM[player].y >= obstacle_y + 6)
			) {
				OAM[player].y -= 1;
			}
		}
		if (keys & KEY_DOWN) 
		{
			if (OAM[player].y < 64
				&& (!effective_speed
					|| !(collision & OBSTACLE_SOLID)
					|| OAM[player].y + 16 <= obstacle_y + 4)
			) {
				OAM[player].y += 1;
			}
		}

		move_map(effective_speed);
		// for(;;) {
		// 	wait_vsync();

		// 	if (~KEY_PAD & KEY_RIGHT) {
		// 		break;
		// 	}
		// }
		// for(;;) {
		// 	wait_vsync();

		// 	if (KEY_PAD & KEY_RIGHT) {
		// 		break;
		// 	}
		// }
	}
}
