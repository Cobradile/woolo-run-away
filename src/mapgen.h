#ifndef __MAPGEN_H__
#define __MAPGEN_H__

#include <stdint.h>

#define COLLIDES_COL 0x10
#define COLLIDES_ROW_RIGHT 0x20
#define OVERLAPS_ROW_ABOVE 0x40
#define OBSTACLE_SOLID 0x80
#define OBSTACLE_IDX 0x0F

void swap_grays();
void init_map(uint16_t seed);
void move_map(uint8_t pixels);

void exchange_obstacle(uint8_t player_id, uint8_t obstacle_id);
uint8_t obstacle_collides(uint8_t x, uint8_t y, uint8_t w, uint8_t h, uint8_t speed);
uint8_t generate_obstacle(uint8_t idx);
void allocate_obstacle(uint8_t idx);
int8_t obstacle2oam(uint8_t idx);

#endif // __MAPGEN_H__
