TOOLCHAIN_DIR := ../..
TARGET = wooloo

C_SOURCES = src/isr.c src/main.c src/sprites.c src/mapgen.c src/wyhash16.c src/debug.c
ASM_SOURCES = src/startup.asm

include $(TOOLCHAIN_DIR)/pm.mk
